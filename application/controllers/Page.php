<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function index()
	{
		$this->load->view('beranda');
	}
	
		public function About()
	{
		$this->load->view('About');
	}
	
		public function Contact()
	{
		$this->load->view('Contact');
	}
}
